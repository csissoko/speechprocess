# AUTHOR : Cheick Mahady SISSOKO
# make configure pour configurer les programmes ALIZE, LIA et SPRO.
# make install pour l'installation complete.

configure:
	./configureAll;
	@echo "Done."

install:
	./installAll;
	@echo "Done."

clean:
	./cleanAll;
	@echo "Done."
