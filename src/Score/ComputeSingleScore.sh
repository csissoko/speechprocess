#!/bin/bash
# Ce Script prend trois arguments, une liste de fichiers prm a tester, un gmm sons, un gmm parole
# Puis, il execute le programme PRM_llk deux fois, une fois pour tester l'appartenance à la classe de son et la deuxieme pour #l'appartenance à la classe de Parole.


gmms=$1
prm=$2
#score=$3
#prmClass=$4
if [ $# -lt 2 ]
then
	echo "Ce programme prend trois parametres."
	echo "Exemple : "
	echo "\$ ComputeScore repertoire_gmms fichier.prm"
	exit 0;
fi
echo "GMM    PRM    Score" > $score.llk
for gmm in `ls $gmms`
do
	nomGmm=`basename $gmm .xml`
	prms=`dirname $prm`
	nomPrm=`basename $prm .prm`
	args="--config=config.cfg --f_o=xml --0=$nomGmm --1=$nomPrm --2=$gmms --3=$prms";
	# Initialisation de la configuration.
	echo $args
	ConfigManager $args
	#sleep 1
	if [ ! -f score.tmp.llk ]
	then
		touch score.tmp.llk;
	fi
	Score --config config.xml >> Score.err
	cat score.tmp.llk >> $nomPrm.score.llk
	rm score.tmp.llk;
done
# Evalution

