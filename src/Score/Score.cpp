/*
 * =====================================================================================
 *
 *       Filename:  Score.cpp
 *
 *    Description:  calcul de LLK à partir d'un modèle GMM 
 * 				utilisant la librairie ALIZE
 *
 * 				C++ compatible GCC-3
 *
 * 	Michel Vacher - 19 décembre 2005
 * 	Bachar Nejem - 03 Juin 2013
 *        Version:  1.0
 *        Created:  25.04.2014 15:04:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Cheick Mahady SISSOKO (CMS), cheickm.sissoko@gmail.com
 *   Organization:  GTALP
 *
 * =====================================================================================
 */

#define NoDEBUG
#define NOMPROG "Score"
#define ALIZE_VERSION "3.0"

#include <iostream>

#include "alize.h"

#include "version.h"


int help(void) {
	cout<<"Message d'aide (programme "<<NOMPROG<<") :"<<endl;
      	cout<<"(calcul de LLK à partir de modèles GMM utilisant ALIZE_"<<ALIZE_VERSION<<")"<<endl;			  
      	cout<<"\t--help\t\t:affichage de l'aide (argument unique)"<<endl;
      	cout<<"\t--version\t: affichage de la version et de la date de compilation (arg. unique)"<<endl;
	cout<<"\t--debug\t\t: mode \"debug\""<<endl;
	cout<<"\t--nomGmm\t\t\t: nom du fichier GMM"<<endl;
	cout<<"\t\t\t\t\t(exemple : c1, gmm/c2, ..."<<endl;
	cout<<"\t\t\t\t\t ne surtout pas mettre l'extension)"<<endl;
	cout<<"\t--loadMixtureFileFormat\t\t: format du fichier GMM (RAW ou XML )"<<endl;
	cout<<"\t--nomPrm\t\t\t: nom du fichier PRM à évaluer"<<endl;
	cout<<"\t\t\t\t\t(exemple : c1_256, prm/c1_256, ..."<<endl;
	cout<<"\t\t\t\t\t idem, ne pas mettre l'extension)"<<endl;
	cout<<"\t--loadFeatureFileFormat\t\t: format des PRM (RAW, SPRO3, SPRO4 ou HTK )"<<endl;
	cout<<"\t\t\t\t\t(SPRO3 pour format ELISA)"<<endl;
	cout<<"\t--loadFeatureFileExtension\t: extension des fichiers (.norm ou .prm)"<<endl;
	cout<<"\t--featureServerMemAlloc\t\t: mémoire allouée à ALIZE (100000)"<<endl;
	cout<<"\t--vectSize\t\t\t: taille des vecteurs acoustiques (16)"<<endl;
	cout<<"\tLe résultat est enregistré dans le fichier \"./score.llk\""<<endl;
	return 0;
}


int main(int argc, char* argv[])
{
	// espace de noms standard
	using namespace std;
	// espace de noms alize
	using namespace alize;

	#if !defined NoDEBUG
	cout <<NOMPROG<<" : debug mode"<< endl << endl;
	#endif

	try
	{
		// Lecture des options d'entrée du programme
		CmdLine cmdLine(argc, argv);
	
		// option --help?
		// fonction disponible en standard dans ALIZE
    		if (cmdLine.displayHelpRequired())
    		{
      			help();
		}    
    		// option --help?
		// fonction disponible en standard dans ALIZE
		if (cmdLine.displayVersionRequired())
    		{
      			cout<<"   Programme "<<NOMPROG<<" : Version: ";
      			cout<<VERSION_NUMBER<<" "<<endl;
      			cout<<"(date de compilation : "<<VERSION_DAY<<"/"<<VERSION_MONTH<<"/";
      			cout<<VERSION_YEAR<<" à "<<VERSION_HOUR<<"h"<<VERSION_MINUTE;
			cout<<" - version ALIZE : "<<ALIZE_VERSION")"<<endl;
    		}
		// Début du programme proprement dit ici! 
    	
		// Initialisation de la configuration
		if(cmdLine.getOptionCount() < 1) 
		{
			help();
			return 1;
		} else {
			if(cmdLine.getName(0) != "config") {
				cout<<"Le premier paramètre doit être le fichier de configuration --config=config.xml"<<endl;
				return 1;
			}
		}
		String configFile=cmdLine.getContent(0);
            	// Initialisation de la configuration
        	Config configTmp(configFile); 
		//String prmType = configTmp.getParam("prmType");

		#include <fstream>
		// fichier prm à évaluer

		String nomPrmATester = configTmp.getParam("featureFilesPath")+configTmp.getParam("nomPrm")+configTmp.getParam("loadFeatureFileExtension");
		String nomPrm = configTmp.getParam("nomPrm");
		fstream fichierPrm(nomPrmATester.c_str(),ios_base::in);
		if (fichierPrm==NULL) {
			//throw Exception(
		  	//		nomPrmATester.c_str(), __LINE__ );
			cout<<"Fichier prm introuvable."<<endl;
			
			return 1;
		}
		fichierPrm.close(); 
		
		// Modèle GMM
		
		MixtureServer ms(configTmp);
		String inputWorldFilename = configTmp.getParam("nomGmm") ;
    		MixtureGD& worldModel = ms.loadMixtureGD(inputWorldFilename);
		
		// Modèle de statistiques
		StatServer ss(configTmp, ms);
    		MixtureStat& worldStat = ss.createAndStoreMixtureStat(worldModel);
		// Paramètres acoustiques
    		// Prise en compte du corpus global
       		// le fichier est une liste de prm, il a pour extension .lst	
    		
		
    		FeatureServer fs(configTmp, nomPrm);
		Feature f;
		//
		
		// calcul de la vraisemblance du modèle
		while (fs.readFeature(f))
        	{
      			if (f.isValid() )
			{
		       		worldStat.computeAndAccumulateLLK(f);
			}
        	}
		double llkResult = worldStat.getMeanLLK() ;
      		
		//cout << llkResult << endl;
		// fichier résultat LLK
		// - par défaut le fichier est ouvert en ASCII, sinon mettre
		// openmode=binary
		// - par défaut le fichier est ouvert en début, sinon mettre
		// openmode=app
		string nomLLK = "score.tmp.llk";
		//
		fstream fichierLLK(nomLLK.c_str(),ios_base::out);
		if (fichierLLK==NULL) {
			//throw UnavailableFileException(
			//		nomLLK, __LINE__ );
			cout<<"Fichier llk null"<<endl;
			return 1;
		}
		//cout << inputWorldFilename << "\t" << nomPrm << "\t" << llkResult << "\t" << prmClass << endl;
		fichierLLK << inputWorldFilename << "\t" << nomPrm << "\t" << llkResult << endl;
		//<t_�X>fichierLLK.close(); 	
		// Fin du programme proprement dit ici.	
	}
	// Traitement des erreurs utilisateur et paramètrage
	catch (Exception& err) {
		cout<<err.msg<<":::"<<err.sourceFile<<":::"<<err.line<<endl;	
	}
}

