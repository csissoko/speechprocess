#!/bin/bash
# Ce Script prend trois arguments, une liste de fichiers prm a tester



gmms=$1
prms=$2
score=$3
#prmClass=$4
if [ $# -lt 3 ]
then
	echo "Ce programme prend trois parametres."
	echo "Exemple : "
	echo "\$ ComputeScore repertoire_gmms repertoire_prm_a_tester fichier_resultat_de_sortie"
	exit 0;
fi
echo "GMM    PRM    Score" > $score.llk
for gmm in `ls $gmms`
do
	nomGmm=`basename $gmm .xml`
	for prm in `ls $prms`
	do
		nomPrm=`basename $prm .prm`
		args="--config=config.cfg --f_o=xml --0=$nomGmm --1=$nomPrm --2=$gmms --3=$prms";
		# Initialisation de la configuration.
		echo $args
		ConfigManager $args
		sleep 1
		if [ ! -f score.tmp.llk ]
		then
			touch score.tmp.llk;
		fi
		Score --config config.xml >> Score.err
		
		cat score.tmp.llk >> $score.llk
		rm score.tmp.llk;
	done
done

# Evalution

