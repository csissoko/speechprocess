
#if !defined(GEOD_Entrainement_cpp)

#define GEOD_Entrainement_cpp


//#include <iostream>
#include <cmath>
#include "MTrainMAP.h"

using namespace alize;
using namespace std;

//static bool verbose = true;

/**************************************************************************
 *
 * Utilis� pour s�lectionner un pourcentage donn� de trames de
 * mani�re al�atoire.
 *
 * Origine : ALIZE
 *
 * Nouvelle �criture car ne marche pas du tout :
 * Michel Vacher - 19 d�cembre 2005
 * 
 * ***********************************************************************/
bool baggedFrame(float baggedFrameProbability)
{
	double db;
  	db = (double(rand())/RAND_MAX)*2. ;
  	if (db  < 2. * baggedFrameProbability )
	  	return (true) ;
  	return (false);
}
/**************************************************************************
 *
 * Fonction annexe utilis�e pour le contr�le de la variance.
 *
 * Origine : ALIZE
 * 
 * ***********************************************************************/
/* float setItParameter(float begin, float end, int nbIt, int it)
{
  	if (nbIt<2) return begin;
  	float itVal=(begin-end/(nbIt-1));
  	return begin-(itVal*it);
} */
/**************************************************************************
 *
 * Recadrage autoritaire de la variance d'un mod�le Gaussien diagonal
 * Utilit� r�elle et justification pour nous?
 *
 * Emploi d�conseill�. Michel Vacher - 16 d�cembre 2005.
 *
 * Origine : ALIZE
 * 
 * ***********************************************************************/
/*  void varianceControl(MixtureGD& model,float flooring,float ceiling)
{
  	unsigned long vectSize   = model.getVectSize();
  	unsigned long distribCount = model.getDistribCount();
  	unsigned long cptFlooring  = 0;
  	unsigned long cptCeiling   = 0;

  	for (unsigned long c=0; c<distribCount; c++)
  	{
    		DistribGD& d = model.getDistrib(c);
    		for (unsigned long v=0; v<vectSize; v++)
    		{
      			float cov = d.getCov(v); 
      			if (cov < flooring) { cov = flooring; cptFlooring++; }
      			if (cov > ceiling)  { cov = ceiling;  cptCeiling++; }
      			d.setCov(cov, v);
    		}
  	}
  	model.computeAll();
  	if (verbose) 
		cout << " total variance flooring = " << cptFlooring << " ceiling = " << cptCeiling << endl;
}*/

/**************************************************************************
 *
 * Entrainement type E.M.
 *
 * (inspir� de l'exemple LIA_RAL)
 * 
 * Michel Vacher - 16 d�cembre 2005.
 *
 * Modification :	30 janvier 2006
 * 
 * 	nbTrainIt et nbTrainFinalIt pass�s cen param�tre programme
 * 
 * ***********************************************************************/
MTrainMAP::MTrainMAP(Config& config)
{
	// Prise en compte des param�tres du programme
  	// Fichiers prm pour apprentissage (liste)
  	const String inputPrmFilename = config.getParam("listePrmPasse2");
  	// nom du mod�le GMM non optimis� obtenu apr�s l'initialisation
  	const String inputWorldFilename = config.getParam("inputWorldFilename");
  
  	// nom du mod�le GMM r�sultat
  	const String outputWorldFilename = config.getParam("nomGmm");
  
  	// probabilit� de conservation des trames 20%
  	float baggedFrameProbability = 0.2f;
  
  	// nombre total de passes d'entrainement
  	// premi�re valeur test�e d�c.05
  	// int nbTrainIt = 20; // avec les 20% du corpus choisis al�atoirement
  	// int nbTrainIt = 24;	// 5 jan.06
  	// Passage par param�tre lors de l'appel programme 30 janvier 2006
	/*
	const String c1 = config.getIntegerParam("nbIterPasse1");
	int i1 = atoi(c1);
	c1 = config.getIntegerParam("nbIterPasse2") ;
	i1+=atoi(c1);
	int nbTrainIt = i1; //config.getIntegerParam("nbIterPasse1") + config.getIntegerParam("nbIterPasse2") ;
	*/
	int nbTrainIt = config.getIntegerParam("nbTrainIt") + config.getIntegerParam("nbIterPasse2") ;


  
  	// nombre de passes finales 
  	// premi�re valeur test�e d�c.05
  	// int nbTrainFinalIt = 10; // tout le corpus pris en compte uniquement 
  				// dans le seuillage
  	// int nbTrainFinalIt = 12; // 6 jan.06 
  	// Passage par param�tre lors de l'appel programme 30 janvier 2006
  	int nbTrainFinalIt = config.getIntegerParam("nbIterPasse2") ;
  
  	// nombre d'it�rations pour l'algorithme E.M. (total number=nbTainIt*nbEmIt)
  	int nbEmIt = 2; // et non 5, avec ELISA c'�tait 1
  
  	// �tiquette des trames s�lectionn�es,
  	// aucune utilisation de label dans ce programme
  	unsigned long labelSelectedFrame = 0;
   
  	// niveau d'information et de debogue
  	bool debug = false;
   	verbose = false;
   	if(config.existsParam("debug"))
		debug = config.getParam("debug").toBool();
	if(config.existsParam("verbose"))
		verbose = config.getParam("verbose").toBool();

  	try
  	{
    		cout << endl;
    		cout << "***********************************" << endl;
    		cout << "* Entra�nement du mod�le gmm (MAP) *" << endl;
    		cout << "***********************************" << endl<<endl;


    		if (verbose) 
	    		cout << "Prise en compte de l'ensemble " << "des param�tres acoustiques du corpus" << endl;    
    
    		// Prise en compte du corpus global
    		FeatureServer fs(config, inputPrmFilename);
    		// le fichier est une liste de prm, il a pour extension .lst 
 
    		if (verbose) 
	    		cout << "Initialisation des GMM" << endl;
    		MixtureServer ms(config);

    		if (verbose) 
	    		cout << "Initialisation des statistiques" << endl;
    		StatServer ss(config, ms);

    		// ajout des fichiers d'�tiquette si n�cessaire
    		// LabelServer labelServer;
	
    		// R�cup�ration des donn�es de l'�tape #K-Means#
    		if (verbose) 
	    		cout << "Chargement des mod�les obtenus pendant l'�tape pr�c�dente" << endl;
    		if (verbose) 
	    		cout << "(fichier interm�diaire : "<<inputWorldFilename<<")" << endl;
    		MixtureGD& worldModel = ms.loadMixtureGD(inputWorldFilename);
            	MixtureGD& client = ms.duplicateMixture(worldModel);
            	MixtureStat& emAcc = ss.createAndStoreMixtureStat(client);
    
    		// M�thode E.M.
    		if (verbose) 
	   	 	cout << "*** Apprentissage des mod�les - premi�res it�rations" << endl;   
    		for (int trainIt=0; trainIt < nbTrainIt; trainIt++)
    		{
			Feature f;

	
			if (verbose) 
				cout << "* It�ration de l'apprentissage : n� " << trainIt + 1 << endl;	  
      
			// It�ration de l'algorithme E.M.
			for (unsigned long emIt=0; emIt<nbEmIt; emIt++)
			{
				if (verbose) cout << "  > E.M. n�"<<emIt<< "  > Mise � 0 des accumulateurs EM" << endl;  
	      				emAcc.resetEM();
        			if (verbose) 
					cout << "  > Calcul and accumulation de EM pour"
                  				<< " chaque param�tre acoustique" << endl;
				// r�initialisation du serveur de statistiques en d�but de
				// traitement
				fs.reset();
	  	
	  			// Lecture et traitement trame par trame des param�tres acoustiques
	  			// sur l'ensemble du corpus examin�
				int nbtram = 0;
				int nbttram = 0;
				bool accept = false;
    	  			while (fs.readFeature(f))     	  
				{
					nbttram++;
					if ( trainIt < (nbTrainIt - nbTrainFinalIt) ) 
					{
						accept = baggedFrame(baggedFrameProbability);
						// cout<<"accept="<<accept<<endl;
      	    					if (f.isValid() && accept )
						{
	              					emAcc.computeAndAccumulateEM(f);
							nbtram++;
						}
					} 
					else
					{
      	    					if (f.isValid() )
						{
	              					emAcc.computeAndAccumulateEM(f);
							nbtram++;
						}
					}

				}
        			if (verbose) 
					cout << "  > Prise en compte du r�sultat ("<<nbtram<<"/"<<nbttram<<" trames)" << endl;
				client = emAcc.getEM();
			
				unsigned long frameCount=(unsigned long) emAcc.getEMFeatureCount();
			
				//computeMAP(MixtureServer &ms,const MixtureGD& initModel,MixtureGD &client,unsigned long frameCount,Config &config)
				//client = MAP(worldModel, client);
				if (verbose) 
					cout<<"Debut calcul MAP ("<<emIt<<")"<<endl;
                		computeMAP(ms, worldModel, client, frameCount, config);
				if (verbose) 
					cout<<"Fin de calcul MAP ("<<emIt<<")"<<endl;
				// Affichage de la vraisemblance du mod�le en mode verbeux
				/*if (verbose) 
				{
					// r�initialisation du serveur de statistiques
					//fs.reset();
					// calcul de la vraisemblance du mod�le
					while (fs.readFeature(f))
        				{
      		    				if (f.isValid() )
						{
		              				emAcc.computeAndAccumulateLLK(f);
						}
        				}
					cout << "It: " << trainIt << " vraisemblance moyenne estim�e du mod�le = "
           					<< emAcc.getMeanLLK() << endl;
    				}*/


    			// fin de la boucle for
    			}
	
    			// Fin de l'apprentissage
    			if (verbose) 
	    			cout << "*** Sauvegarde du mod�le appris (" << outputWorldFilename << ")" << endl;
    			client.save(outputWorldFilename, config);
  		}
	}
	// Traitement des exceptions standard
	catch (exception& err){
		//throw GenericAlizeException( err , __FILE__ , __LINE__ );
		std::cerr <<"###"<<__FILE__ <<"::"<<__LINE__<< endl;
		std::cerr <<endl<<"### "<<"Entrainement"<<" ### --- $$$ EXCEPTION STANDARD $$$\n";
      	  	std::cerr << "### "<<endl<< err.what() <<endl<<" ###"<<endl;
	} 
	catch (FileNotFoundException& err) {
		cout<<"Erreur fichier introuvable."<<endl;
		cout<<err.msg<<":::"<<err.fileName<<":::"<<err.sourceFile<<":::"<<err.line<<endl;	  
	}
	catch(Exception& err) {
		cout<<"Erreur Excepetion."<<endl;
		cout<<err.msg<<":::"<<err.sourceFile<<":::"<<err.line<<endl;
	}

}
//-------------------------------------------------------------------------
MTrainMAP::~MTrainMAP() {}
//-------------------------------------------------------------------------

#endif


