/*******************************************************************************
 *
 * Programme GMM_Train : 	apprentissage de Gaussian Mixture Model
 * 				utilisant la librairie ALIZE
 *
 * 				C++ compatible GCC-3
 *
 * Michel Vacher - 20 octobre 2005
 *
 * modifications : 30 janvier 2006
 *	passage par param�tre de nbTrainIt et nbFinalTrainIt
 *
 *
 * Cheick Mahady SISSOKO
 * modifications : 24 Avr. 2014
 * 	Passage du fichier configuration en param�tre.
 *
 *
 ******************************************************************************/
#define NoDEBUG
#define NOMPROG "Training"
#define ALIZE_VERSION "3.0"


#include <iostream>
#include <string>

#include "MTrainInit.h"
#include "MTrainEM.h"
#include "MTrainMAP.h"

#include "version.h"

int help(void) {
	cout<<"Message d'aide (programme "<<NOMPROG<<") :"<<endl;
	cout<<"(apprentissage de mod�les GMM utilisant ALIZE_"<<ALIZE_VERSION<<")"<<endl;
        cout<<"\t--help\t\t:affichage de l'aide (argument unique)"<<endl;
        cout<<"\t--version\t: affichage de la version et de la date de compilation (arg. unique)"<<endl;
   	cout<<"\t--debug\t: mode \"debug\""<<endl;
	cout<<"\t--mode\t: le mode d'entrainement soit par EM ou MAP"<<endl;
        cout<<"\t--config\t: fichier de configuration "<<endl;
        cout<<"\t\t\t  ordre de priorit�, d'abord ligne de commande, ensuite "<<endl;
   	cout<<"\t\t\t  fichier de configuration, ensuite seulement les"<<endl;
        cout<<"\t\t\t  param�tres par d�faut"<<endl;
        cout<<"\t--mixtureDistribCount\t: nombre de gaussiennes pour chaque GMM (4)"<<endl;
        cout<<"\t--distribType\t: type de matrice (GD=diagonale, GF=pleine)"<<endl;
        cout<<"\t--mixtureFilesPath\t: chemin d'acc�s (./)"<<endl;
	cout<<"\t--inputWorldFilename\t: l'ubm pour l'entrainement MAP."<<endl;
        cout<<"\t--nomGmm\t: nom du fichier GMM (t)"<<endl;
        cout<<"\t\t\t(ici mettre l'extension)"<<endl;
        cout<<"\t--saveMixtureFileFormat\t: format du fichier GMM (RAW ou XML )"<<endl;
        cout<<"\t--saveMixtureFileExtension\t: extension du fichier (.gmm ou .xml)"<<endl;
        cout<<"\t--featureFilesPath\t: chemin des fichiers PRM (./prm/)"<<endl;
       	cout<<"\t--loadFeatureFileFormat\t: format des PRM (RAW, SPRO3, SPRO4 ou HTK )"<<endl;
      	cout<<"\t\t\t(SPRO4 pour format ELISA)"<<endl;
       	cout<<"\t--loadFeatureFileExtension\t: extension des fichiers (.norm ou .prm)"<<endl;
        cout<<"\t--featureServerMemAlloc\t: m�moire allou�e � ALIZE (100000)"<<endl;
        cout<<"\t--vectSize\t: taille des vecteurs acoustiques (24)"<<endl;
        cout<<"\t--listePrmPasse1\t: liste des PRM pour initialiser EM (./liste1.lst)"<<endl;
        cout<<"\t\t\t(ne pas mettre l'extension � la fin des noms)"<<endl;
        cout<<"\t--listePrmPasse2\t: liste des PRM pour apprentissage EM (./liste2.lst)"<<endl;
        cout<<"\t\t\t(ne pas mettre l'extension � la fin des noms)"<<endl;
        cout<<"\t--nbTrainIt\t: nombre de passes EM avec 20\% du corpus"<<endl;
        cout<<"\t--nbIterPasse2\t: nombre de passes EM avec 100\% du corpus"<<endl;
        cout<<"Fichier ./bande.ini, non obligatoire mais prioritaire sur la configuration"<<endl;
        cout<<"par --vectSize ou config.ini, doit contenir au minimum une ligne :"<<endl;
        cout<<"nbparam <valeur> ou"<<endl;
        cout<<"nbparam\\t<valeur> , soit par exemple :"<<endl;
        cout<<"nbparam 24"<<endl;
	return 0;
}

int main(int argc, char* argv[])
{
	// espace de noms standard
    	using namespace std;
	// espace de noms alize
    	using namespace alize;

	#if !defined NoDEBUG
    	cout <<NOMPROG<<" : debug mode"<< endl << endl;
	#endif

    	try
    	{
        	// Lecture des options d'entr�e du programme
        	CmdLine cmdLine(argc, argv);
		// option --help?
        	// fonction disponible en standard dans ALIZE
        	if (cmdLine.displayHelpRequired())
        	{
			help();
        	}
        	// option --help?
        	// fonction disponible en standard dans ALIZE
		else if (cmdLine.displayVersionRequired())
        	{
           		cout<<"   Programme "<<NOMPROG<<" : Version: ";
            		cout<<VERSION_NUMBER<<" "<<endl;
            		cout<<"(date de compilation : "<<VERSION_DAY<<"/"<<VERSION_MONTH<<"/";
            		cout<<VERSION_YEAR<<" � "<<VERSION_HOUR<<"h"<<VERSION_MINUTE;
            		cout<<" - version ALIZE : "<<ALIZE_VERSION")"<<endl;
        	}

        	else
            	// D�but du programme proprement dit ici!
        	{
            		// R�pertoire de travail par d�faut
            		String baseDir = "./";  
			if(cmdLine.getOptionCount() < 1) 
			{
				help();
				return 1;
			} else {
				if(cmdLine.getName(0) != "config") {
					cout<<"Le premier param�tre doit �tre le fichier de configuration --config=config.xml"<<endl;
					return 1;
				}
			}
			String configFile=cmdLine.getContent(0);
			cout<<configFile<<endl;
            		// Initialisation de la configuration
        		Config configTmp(configFile);  
			//cout<<configTmp.toString()<<endl;

            		// Initialisation de l'algorithme EM

            		String mode = "EM";
			if(configTmp.existsParam("mode")) {
				mode = configTmp.getParam("mode");
			}
			cout<<"Mode Entrainement : "<<mode<<endl;
			if(mode == "EM") {
				cout<<"===============Initialisation de l'entrainement.============="<<endl;
            			MTrainInit init(configTmp);
            			//
            			// Entrainement 2� passe
            		
				cout<<"==================Entrainement proprement dit par la methode EM.================"<<endl;
            			MTrainEM train(configTmp);
            			// WorldInit init(config);
            			// WorldTraining train(config);
            			cout <<endl<<"------bach-------- "<<NOMPROG<<" Fin de "<<NOMPROG;
            			cout<<" -----------" << endl << endl;
			} else if(mode == "MAP") {
				String inputWorldFilename;
				cout<<"World : " <<inputWorldFilename<<endl;
				if(!configTmp.existsParam("inputWorldFilename")) {
					cout<<"Le parametre inputWorldFilename est requis"<<endl;
					return 1;
				}
				inputWorldFilename = configTmp.getParam("inputWorldFilename");
				cout<<"==================Entrainement proprement dit par la methode MAP.================"<<endl;
            			MTrainMAP train(configTmp);
			} else {
				cout<<"Mode inconnue"<<endl;
			}
		}
		// Fin du programme proprement dit ici.
    	}
	// Traitement des exceptions standard
    	catch (exception& err) {
       	 	std::cerr <<endl<<"### "<<NOMPROG<<" ### --- $$$ EXCEPTION STANDARD $$$\n";
        	std::cerr << "### "<<endl<< err.what() <<endl<<" ###"<<endl;
    	}
	catch (FileNotFoundException& err) {
		cout<<"Erreur fichier introuvable."<<endl;
		cout<<err.msg<<":::"<<err.fileName<<":::"<<err.sourceFile<<":::"<<err.line<<endl;	  
	}
	catch(Exception& err) {
		cout<<"Erreur Excepetion."<<endl;
		cout<<err.msg<<":::"<<err.sourceFile<<":::"<<err.line<<endl;
	}
    	return 0;
}
