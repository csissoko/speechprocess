#!/bin/bash
#1- verification de l'existence des fichiers PRM, la liste des fichiers correspondants.
#2- creation de fichier de configuration XML
#3- execution de programme GMM_Train
#4- sauvegarde de resultat

# - FileLst :
#		Ce script execute un programme java donc il faut avoir au moins java 6 pour que le programme marche.
#		Il prend en argument les options suivantes
#		--absolute: 
#			Si cette option est indiquée il doit etre le premier argument.
#			Ca signifique que la liste des fichiers doit etre constituee des chemins absolus.
#			Sinon la liste sera constituée des chemin relative par rapport au repertoire passé en argument $1.
#		Repertoire Parent:
#			Voir le programme FileLst $ FileLst entree pour de l'aide.
#		Le fichier de sortie:
#			La liste des noms des fichiers.
#			S'il n'est pas indiqué alors le fichier de sortie sera le nom_du_repertoire_parent.lst.


if [ "$1" = "" ]
then
    echo "veuillez saisir le nom de répértoire contenant les fichiers PRM"
    exit
fi
#Script_Lister_CL.sh fait l'étape 1

list="$PWD/list"
if [ ! -d "$list" ] 
then
  echo "Making Lists Directory"
  mkdir list
fi
PRMDIR="$PWD/PRM/$1"
if [ ! -d "$PRMDIR" ] 
then
  echo "No Prm Directory named $1 "
  exit
fi
#for i in `ls PRM/$1`  
#do  
#    echo "`pwd`/PRM/$1/$i" >> list/list_$1.lst
#    echo "`pwd`/PRM/$1/`basename $i .prm`" >> list/train_gmm_$1.lst
#done
# Création des lists
FileLst --absolute $PWD/PRM/$1 $list/list_$1.lst
FileLst --absolute $PWD/PRM/$1 $list/train_gmm_$1.lst .prm

#Modification des parametres dans le fichier config.xml

listeExtension="$PWD/list/list_$1.lst"
listSansExtension="$PWD/list/train_gmm_$1.lst"
args="--config=config.cfg --f_o=xml --0=$1 --1=$listeExtension --2=$listeExtension";
echo $args;
# Initialisation de la configuration.
ConfigManager $args

sleep 1
#Ecexution du Programme GMM_Train

Training --config config.xml

sleep 1
#Creation de repertoire GMM

if [ ! -d "GMM" ] 
then
  mkdir GMM
fi

#Deplacer le resultat
#rm "$PWD/list/list_$1.lst"
#rm "$PWD/list/train_gmm_$1.lst"

#mv "$1.xml" GMM/
#rm t1.txt
echo "FIN!"

