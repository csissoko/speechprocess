
#if !defined(GEOD_Initialisation_cpp)
#define GEOD_Initialisation_cpp

#include <iostream>
#include "MTrainInit.h"

using namespace alize;
using namespace std;

//-------------------------------------------------------------------------
MTrainInit::MTrainInit(Config& config)
{
 	 try
 	 {
    		cout << endl;
    		cout << "*************************************" << endl;
    		cout << "* Initialisation du mod�le avant EM *" << endl;
   	 	cout << "*************************************" << endl;
    		cout << endl;
    		cout << "Param�tres acoustiques pour initialisation" << endl;
   
    		cout<< "distribType = "<<config.getParam("distribType")<<endl;
    		//FeatureServer fs(config, "../data/prm/c3_xx_passe1.lst");

   		//verification de config
   		cout<< "distribType = "<<config.getParam("distribType")<<endl;
   		cout<< "mixtureDistribCount = "<<config.getParam("mixtureDistribCount")<<endl;
		cout<< "maxLLK = "<<config.getParam("maxLLK")<<endl;
		cout<< "minLLK = "<<config.getParam("minLLK")<<endl;
		cout<< "saveMixtureFileFormat = "<<config.getParam("saveMixtureFileFormat")<<endl;
		cout<< "loadMixtureFileFormat = "<<config.getParam("loadMixtureFileFormat")<<endl;
		cout<< "loadFeatureFileFormat = "<<config.getParam("loadFeatureFileFormat")<<endl;
		cout<< "featureServerBufferSize = "<<config.getParam("featureServerBufferSize")<<endl;
		cout<< "loadMixtureFileExtension = "<<config.getParam("loadMixtureFileExtension")<<endl;
		cout<< "saveMixtureFileExtension = "<<config.getParam("saveMixtureFileExtension")<<endl;

		cout<< "loadFeatureFileExtension = "<<config.getParam("loadFeatureFileExtension")<<endl;
		cout<< "featureFilesPath = "<<config.getParam("featureFilesPath")<<endl;
		cout<< "mixtureFilesPath = "<<config.getParam("mixtureFilesPath")<<endl;
		cout<< "wavFilesPath = "<<config.getParam("wavFilesPath")<<endl;
		cout<< "vectSize = "<<config.getParam("vectSize")<<endl;
		cout<< "listeGmm = "<<config.getParam("listeGmm")<<endl;
		cout<< "listePrmATester = "<<config.getParam("listePrmATester")<<endl;
		cout<< "nomGmm = "<<config.getParam("nomGmm")<<endl;
		cout<< "listePrmPasse1 = "<<config.getParam("listePrmPasse1")<<endl;
		cout<< "listePrmPasse2 = "<<config.getParam("listePrmPasse2")<<endl;
		cout<< "debug = "<<config.getParam("debug")<<endl;
   		//fin Verification config

    		//Verification des arguments pass�s au constructeur de FeatureServer
    		cout << "Config : " << config.toString() << endl;
    		cout << "listePrmPasse1 : " <<config.getParam("listePrmPasse1")  << endl;  
    		//Fin de Verification 
    		String listprm = config.getParam("listePrmPasse1");
    		FeatureServer fs(config, listprm );
    		cout<<fs.toString()<<endl;


    		cout << "Cr�ation du mod�le de m�lange" << endl;

   	 	MixtureServer ms(config);
    		cout<<ms.toString()<<endl;

    		cout << "Cr�ation des GMM dans le mod�le de m�lange" << endl;

    		MixtureGD& world = ms.createMixtureGD();

    		cout << "Cr�ation du serveur de statistiques" << endl;

    		StatServer ss(config, ms);

    		cout << "Calcul/initialisation des moyennes et �cart-types des distributions" << endl;
    
    		unsigned long c, v;
    		cout << "avant getFeatureCount" << endl;
    		unsigned long nbTrames = fs.getFeatureCount();
        	cout << "apr�s getFeatureCount" << endl;
   		unsigned long vectSize = world.getVectSize();
    		unsigned long distribCount = world.getDistribCount();

    		unsigned long step = nbTrames / distribCount;
   	 	unsigned long nb = 10;
    		// Calcul d'arrondi
    		if (nb > nbTrames/(step+1))
      			nb = nbTrames/(step+1);

    		for (v=0; v<vectSize; v++)
    		{
      			real_t sum = 0.0;
      			real_t sumsqr = 0.0;

      			fs.seekFeature(0);
      			Feature f;
      			while (fs.readFeature(f))
      			{
        			real_t data = f[v];
        			sum += data;
        			sumsqr += data * data;
      			}
      			real_t cov  = sumsqr / nbTrames - (sum / nbTrames) * (sum / nbTrames);

      			for (c=0; c < distribCount; c++)
      			{
        			DistribGD& d = world.getDistrib(c);
        			real_t cumul = 0.0;
        			fs.seekFeature(c*step);
    				unsigned long n;
        			unsigned long nn = 0;
        			for (n=0; n<nb; n++)
        			{
          				if (fs.readFeature(f))
          				{
           	 				cumul += f[v];
            					nn++;
          				}
        			}
        			d.setCov(cov, v); // cov must be 1.0
        			d.setMean(cumul / nn, v);
      			}
    		}
    		world.computeAll();

    		cout << "Initialisation du poids des distributions" << endl;
    
    		world.equalizeWeights(); // set weight = 1/distribCount for each distrib

    		cout << "Enregistrement de la GMM provisoire" << endl;

    		world.save("temp", config);
  	}
  	// Traitement des exceptions standard
  	catch (exception& err) {
		std::cerr <<endl<<"### "<<"Initialisation"<<" ### --- $$$ EXCEPTION STANDARD $$$\n";
        	std::cerr << "### "<<endl<< err.what() <<endl<<" ###"<<endl;
	  	//throw GenericAlizeException( err , __FILE__ , __LINE__ );
  	} 
  	// Traitement des exceptions FileNotFoundException de ALIZE
  	catch (FileNotFoundException& err) {
		cout<<"Erreur fichier introuvable."<<endl;
		cout<<err.msg<<":::"<<err.fileName<<":::"<<err.sourceFile<<":::"<<err.line<<endl;	  
	}
	catch(Exception& err) {
		cout<<"Erreur Excepetion."<<endl;
		cout<<err.msg<<":::"<<err.sourceFile<<":::"<<err.line<<endl;
	}

}
//-------------------------------------------------------------------------
MTrainInit::~MTrainInit() {}
//-------------------------------------------------------------------------
#endif // 


