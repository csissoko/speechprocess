/*****************************************************
 *
 *        Fichier définissant la date de version
 *        Programme Training
 *        Michel Vacher - 20 octobre 2005
 *
 ***
 *
 * Nom de fichier version.h :
 *      génération automatique par Makefile
 *      le Jeu 12 nov 2015 21:56:25 CET
 *
 *****************************************************/

#define VERSION_NUMBER "0.9.0"

#define VERSION_YEAR 2015
#define VERSION_MONTH 11
#define VERSION_DAY 12
#define VERSION_HOUR 21
#define VERSION_MINUTE 56

